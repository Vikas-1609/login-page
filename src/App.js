import React from 'react';
import './App.css'; // Import your global CSS styles
import Login from './Login'; // Import the Login component

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Login /> {/* Render the Login component */}
      </header>
    </div>
  );
}

export default App;
